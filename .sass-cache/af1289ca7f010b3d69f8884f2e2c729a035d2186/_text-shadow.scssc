3.2.12 (Media Mark)
c75b2de362bcd025328342af1ea89a0895009d51
o:Sass::Tree::RootNode
:
@linei:@options{ :@has_childrenT:@templateI"9@import "shared";

// These defaults make the arguments optional for this mixin
// If you like, set different defaults in your project

$default-text-shadow-color:    #aaa !default;
$default-text-shadow-h-offset: 0px  !default;
$default-text-shadow-v-offset: 0px  !default;
$default-text-shadow-blur:     1px  !default;
$default-text-shadow-spread:   false  !default;

// Provides cross-browser text shadows when one or more shadows are needed.
// Each shadow argument should adhere to the standard css3 syntax for the
// text-shadow property.
//
// Note: if any shadow has a spread parameter, this will cause the mixin
// to emit the shadow declaration twice, first without the spread,
// then with the spread included. This allows you to progressively
// enhance the browsers that do support the spread parameter.
@mixin text-shadow(
  $shadow-1 : default,
  $shadow-2 : false,
  $shadow-3 : false,
  $shadow-4 : false,
  $shadow-5 : false,
  $shadow-6 : false,
  $shadow-7 : false,
  $shadow-8 : false,
  $shadow-9 : false,
  $shadow-10: false
) {
  @if $shadow-1 == default {
    $shadow-1: compact($default-text-shadow-h-offset $default-text-shadow-v-offset $default-text-shadow-blur $default-text-shadow-spread $default-text-shadow-color);
  }
  $shadows-without-spread: join((),(),comma);
  $shadows: join((),(),comma);
  $has-spread: false;
  @each $shadow in compact($shadow-1, $shadow-2, $shadow-3, $shadow-4, $shadow-5,
                           $shadow-6, $shadow-7, $shadow-8, $shadow-9, $shadow-10) {
    @if length($shadow) > 4 {
      $has-spread: true;
      $shadows-without-spread: append($shadows-without-spread, nth($shadow,1) nth($shadow,2) nth($shadow,3) nth($shadow,5));
      $shadows: append($shadows, $shadow);
    } else {
      $shadows-without-spread: append($shadows-without-spread, $shadow);
      $shadows: append($shadows, $shadow);
    }
  }
  @if $has-spread {
    text-shadow: $shadows-without-spread;
  }
  text-shadow: $shadows;
}

// Provides a single cross-browser CSS text shadow.
//
// Provides sensible defaults for the color, horizontal offset, vertical offset, blur, and spread
// according to the configuration defaults above.
@mixin single-text-shadow(
  $hoff: false,
  $voff: false,
  $blur: false,
  $spread: false,
  $color: false
) {
  // A lot of people think the color comes first. It doesn't.
  @if type-of($hoff) == color {
    $temp-color: $hoff;
    $hoff: $voff;
    $voff: $blur;
    $blur: $spread;
    $spread: $color;
    $color: $temp-color;
  }
  // Can't rely on default assignment with multiple supported argument orders.
  $hoff:   if($hoff,   $hoff,   $default-text-shadow-h-offset);
  $voff:   if($voff,   $voff,   $default-text-shadow-v-offset);
  $blur:   if($blur,   $blur,   $default-text-shadow-blur    );
  $spread: if($spread, $spread, $default-text-shadow-spread  );
  $color:  if($color,  $color,  $default-text-shadow-color   );
  // We don't need experimental support for this property.
  @if $color == none or $hoff == none {
    @include text-shadow(none);
  } @else {
    @include text-shadow(compact($hoff $voff $blur $spread $color));
  }
}
:ET:@children[o:Sass::Tree::ImportNode:@imported_filenameI"shared;
T;@;i:@imported_file0;	0;[ o:Sass::Tree::CommentNode
:
@type:silent;i;@:@value[I"{/* These defaults make the arguments optional for this mixin
 * If you like, set different defaults in your project */;
T;[ o:Sass::Tree::VariableNode;@:@guardedI"!default;
T;i:
@nameI"default-text-shadow-color;
T:
@expro:Sass::Script::Color	:@attrs{	:redi�:
greeni�:	bluei�:
alphai;i;0;@;[ o;;@;I"!default;
T;i;I"!default-text-shadow-h-offset;
T;o:Sass::Script::Number;@:@numerator_units[I"px;
T;i:@denominator_units[ :@originalI"0px;
F;i ;[ o;;@;I"!default;
T;i;I"!default-text-shadow-v-offset;
T;o;;@;[I"px;
T;i;[ ; I"0px;
F;i ;[ o;;@;I"!default;
T;i;I"default-text-shadow-blur;
T;o;;@;[I"px;
T;i;[ ; I"1px;
F;i;[ o;;@;I"!default;
T;i;I"default-text-shadow-spread;
T;o:Sass::Script::Bool;@;F;i;[ o;
;;;i;@;[I"�/* Provides cross-browser text shadows when one or more shadows are needed.
 * Each shadow argument should adhere to the standard css3 syntax for the
 * text-shadow property.
 *
 * Note: if any shadow has a spread parameter, this will cause the mixin
 * to emit the shadow declaration twice, first without the spread,
 * then with the spread included. This allows you to progressively
 * enhance the browsers that do support the spread parameter. */;
T;[ o:Sass::Tree::MixinDefNode;@;i$;I"text-shadow;
T:
@args[[o:Sass::Script::Variable;I"shadow-1;
T:@underscored_nameI"shadow_1;
T;@o:Sass::Script::String	;I"default;
T;@;:identifier;i[o;$;I"shadow-2;
T;%I"shadow_2;
T;@o;!;@;F;i[o;$;I"shadow-3;
T;%I"shadow_3;
T;@o;!;@;F;i[o;$;I"shadow-4;
T;%I"shadow_4;
T;@o;!;@;F;i[o;$;I"shadow-5;
T;%I"shadow_5;
T;@o;!;@;F;i[o;$;I"shadow-6;
T;%I"shadow_6;
T;@o;!;@;F;i[o;$;I"shadow-7;
T;%I"shadow_7;
T;@o;!;@;F;i [o;$;I"shadow-8;
T;%I"shadow_8;
T;@o;!;@;F;i![o;$;I"shadow-9;
T;%I"shadow_9;
T;@o;!;@;F;i"[o;$;I"shadow-10;
T;%I"shadow_10;
T;@o;!;@;F;i#;T:@splat0;[u:Sass::Tree::IfNodeu[o:Sass::Script::Operation
:@options{ :@operand1o:Sass::Script::Variable	:
@nameI"shadow-1:ET:@underscored_nameI"shadow_1;
T;@:
@linei%;i%:@operand2o:Sass::Script::String	:@valueI"default;
T;@:
@type:identifier;i%:@operator:eq0[o:Sass::Tree::VariableNode;@:@guarded0;i&;	I"shadow-1;
T:
@expro:Sass::Script::Funcall;@;i&:@keywords{ ;	I"compact;
T:
@args[o:Sass::Script::List	:@separator:
space;i&;[
o;	;	I"!default-text-shadow-h-offset;
T;I"!default_text_shadow_h_offset;
T;@;i&o;	;	I"!default-text-shadow-v-offset;
T;I"!default_text_shadow_v_offset;
T;@;i&o;	;	I"default-text-shadow-blur;
T;I"default_text_shadow_blur;
T;@;i&o;	;	I"default-text-shadow-spread;
T;I"default_text_shadow_spread;
T;@;i&o;	;	I"default-text-shadow-color;
T;I"default_text_shadow_color;
T;@;i&;@:@splat0:@children[ o;;@;0;i(;I"shadows-without-spread;
T;o:Sass::Script::Funcall;@;i(:@keywords{ ;I"	join;
T;#[o:Sass::Script::List	:@separator:
space;i(;[ ;@o;,	;-;.;i(;[ ;@o;&	;I"
comma;
T;@;;';i(;(0;[ o;;@;0;i);I"shadows;
T;o;*;@;i);+{ ;I"	join;
T;#[o;,	;-;.;i);[ ;@o;,	;-;.;i);[ ;@o;&	;I"
comma;
T;@;;';i);(0;[ o;;@;0;i*;I"has-spread;
T;o;!;@;F;i*;[ o:Sass::Tree::EachNode;@;i,:
@listo;*;@;i,;+{ ;I"compact;
T;#[o;$	;I"shadow-1;
T;%I"shadow_1;
T;@;i+o;$	;I"shadow-2;
T;%I"shadow_2;
T;@;i+o;$	;I"shadow-3;
T;%I"shadow_3;
T;@;i+o;$	;I"shadow-4;
T;%I"shadow_4;
T;@;i+o;$	;I"shadow-5;
T;%I"shadow_5;
T;@;i+o;$	;I"shadow-6;
T;%I"shadow_6;
T;@;i,o;$	;I"shadow-7;
T;%I"shadow_7;
T;@;i,o;$	;I"shadow-8;
T;%I"shadow_8;
T;@;i,o;$	;I"shadow-9;
T;%I"shadow_9;
T;@;i,o;$	;I"shadow-10;
T;%I"shadow_10;
T;@;i,;(0:	@varI"shadow;
T;T;[u;)[o:Sass::Script::Operation
:@options{ :@operand1o:Sass::Script::Funcall;@:
@linei-:@keywords{ :
@nameI"length:ET:
@args[o:Sass::Script::Variable	;I"shadow;T:@underscored_nameI"shadow;T;@;	i-:@splat0;	i-:@operand2o:Sass::Script::Number;@:@numerator_units[ ;	i-:@denominator_units[ :@originalI"4;F:@valuei	:@operator:gt0[o:Sass::Tree::VariableNode;@:@guarded0;	i.;I"has-spread;T:
@expro:Sass::Script::Bool;@;T;	i.:@children[ o;;@;0;	i/;I"shadows-without-spread;T;o;;@;	i/;
{ ;I"append;T;[o;	;I"shadows-without-spread;T;I"shadows_without_spread;T;@;	i/o:Sass::Script::List	:@separator:
space;	i/;[	o;;@;	i/;
{ ;I"nth;T;[o;	;I"shadow;T;I"shadow;T;@;	i/o;;@;[ ;	i/;@;I"1;F;i;0o;;@;	i/;
{ ;I"nth;T;[o;	;I"shadow;T;I"shadow;T;@;	i/o;;@;[ ;	i/;@;I"2;F;i;0o;;@;	i/;
{ ;I"nth;T;[o;	;I"shadow;T;I"shadow;T;@;	i/o;;@;[ ;	i/;@;I"3;F;i;0o;;@;	i/;
{ ;I"nth;T;[o;	;I"shadow;T;I"shadow;T;@;	i/o;;@;[ ;	i/;@;I"5;F;i
;0;@;0;[ o;;@;0;	i0;I"shadows;T;o;;@;	i0;
{ ;I"append;T;[o;	;I"shadows;T;I"shadows;T;@;	i0o;	;I"shadow;T;I"shadow;T;@;	i0;0;[ o:Sass::Tree::RuleNode;@:
@tabsi ;i1;[o;;@;0;i2;I"shadows-without-spread;
T;o;*;@;i2;+{ ;I"append;
T;#[o;$	;I"shadows-without-spread;
T;%I"shadows_without_spread;
T;@;i2o;$	;I"shadow;
T;%I"shadow;
T;@;i2;(0;[ o;;@;0;i3;I"shadows;
T;o;*;@;i3;+{ ;I"append;
T;#[o;$	;I"shadows;
T;%I"shadows;
T;@;i3o;$	;I"shadow;
T;%I"shadow;
T;@;i3;(0;[ ;T:@parsed_ruleso:"Sass::Selector::CommaSequence:@members[o:Sass::Selector::Sequence;6[o:#Sass::Selector::SimpleSequence
:@subject0:@sourceso:Set:
@hash{ ;i1:@filenameI" ;
F;6[o:Sass::Selector::Element	;[I"	else;
T:@namespace0;i1;=@�;i1;=@�:
@rule[I"	else;
Tu;)$[o:Sass::Script::Variable	:
@nameI"has-spread:ET:@underscored_nameI"has_spread;T:@options{ :
@linei60[o:Sass::Tree::PropNode:
@tabsi :@children[ :@prop_syntax:new:@valueo; 	;I"shadows-without-spread;T;I"shadows_without_spread;T;	@	;
i7;[I"text-shadow;T;
i7;	@	o:Sass::Tree::PropNode;3i ;[ :@prop_syntax:new;o;$	;I"shadows;
T;%I"shadows;
T;@;i9;[I"text-shadow;
T;i9;@o;
;;;i<;@;[I"�/* Provides a single cross-browser CSS text shadow.
 *
 * Provides sensible defaults for the color, horizontal offset, vertical offset, blur, and spread
 * according to the configuration defaults above. */;
T;[ o;";@;iF;I"single-text-shadow;
T;#[
[o;$;I"	hoff;
T;%I"	hoff;
T;@o;!;@;F;iA[o;$;I"	voff;
T;%I"	voff;
T;@o;!;@;F;iB[o;$;I"	blur;
T;%I"	blur;
T;@o;!;@;F;iC[o;$;I"spread;
T;%I"spread;
T;@o;!;@;F;iD[o;$;I"
color;
T;%I"
color;
T;@o;!;@;F;iE;T;(0;[o;
;;;iG;@;[I"C/* A lot of people think the color comes first. It doesn't. */;
T;[ u;)?[o:Sass::Script::Operation
:@options{ :@operand1o:Sass::Script::Funcall;@:
@lineiH:@keywords{ :
@nameI"type-of:ET:
@args[o:Sass::Script::Variable	;I"	hoff;T:@underscored_nameI"	hoff;T;@;	iH:@splat0;	iH:@operand2o:Sass::Script::String	:@valueI"
color;T;@:
@type:identifier;	iH:@operator:eq0[o:Sass::Tree::VariableNode;@:@guarded0;	iI;I"temp-color;T:
@expro;	;I"	hoff;T;I"	hoff;T;@;	iI:@children[ o;;@;0;	iJ;I"	hoff;T;o;	;I"	voff;T;I"	voff;T;@;	iJ;[ o;;@;0;	iK;I"	voff;T;o;	;I"	blur;T;I"	blur;T;@;	iK;[ o;;@;0;	iL;I"	blur;T;o;	;I"spread;T;I"spread;T;@;	iL;[ o;;@;0;	iM;I"spread;T;o;	;I"
color;T;I"
color;T;@;	iM;[ o;;@;0;	iN;I"
color;T;o;	;I"temp-color;T;I"temp_color;T;@;	iN;[ o;
;;;iP;@;[I"T/* Can't rely on default assignment with multiple supported argument orders. */;
T;[ o;;@;0;iQ;I"	hoff;
T;o;*;@;iQ;+{ ;I"if;
T;#[o;$	;I"	hoff;
T;%I"	hoff;
T;@;iQo;$	;I"	hoff;
T;%I"	hoff;
T;@;iQo;$	;I"!default-text-shadow-h-offset;
T;%I"!default_text_shadow_h_offset;
T;@;iQ;(0;[ o;;@;0;iR;I"	voff;
T;o;*;@;iR;+{ ;I"if;
T;#[o;$	;I"	voff;
T;%I"	voff;
T;@;iRo;$	;I"	voff;
T;%I"	voff;
T;@;iRo;$	;I"!default-text-shadow-v-offset;
T;%I"!default_text_shadow_v_offset;
T;@;iR;(0;[ o;;@;0;iS;I"	blur;
T;o;*;@;iS;+{ ;I"if;
T;#[o;$	;I"	blur;
T;%I"	blur;
T;@;iSo;$	;I"	blur;
T;%I"	blur;
T;@;iSo;$	;I"default-text-shadow-blur;
T;%I"default_text_shadow_blur;
T;@;iS;(0;[ o;;@;0;iT;I"spread;
T;o;*;@;iT;+{ ;I"if;
T;#[o;$	;I"spread;
T;%I"spread;
T;@;iTo;$	;I"spread;
T;%I"spread;
T;@;iTo;$	;I"default-text-shadow-spread;
T;%I"default_text_shadow_spread;
T;@;iT;(0;[ o;;@;0;iU;I"
color;
T;o;*;@;iU;+{ ;I"if;
T;#[o;$	;I"
color;
T;%I"
color;
T;@;iUo;$	;I"
color;
T;%I"
color;
T;@;iUo;$	;I"default-text-shadow-color;
T;%I"default_text_shadow_color;
T;@;iU;(0;[ o;
;;;iV;@;[I"@/* We don't need experimental support for this property. */;
T;[ u;)�[o:Sass::Script::Operation
:@options{ :@operand1o; 
;@;o:Sass::Script::Variable	:
@nameI"
color:ET:@underscored_nameI"
color;
T;@:
@lineiW;iW:@operand2o:Sass::Script::String	:@valueI"	none;
T;@:
@type:identifier;iW:@operator:eq;iW;o; 
;@;o;	;	I"	hoff;
T;I"	hoff;
T;@;iW;iW;o;	;I"	none;
T;@;;;iW;;;:oru:Sass::Tree::IfNode�[00[o:Sass::Tree::MixinNode:@options{ :
@args[o:Sass::Script::Funcall;@:
@lineiZ:@keywords{ :
@nameI"compact:ET;[o:Sass::Script::List	:@separator:
space;	iZ:@value[
o:Sass::Script::Variable	;I"	hoff;T:@underscored_nameI"	hoff;T;@;	iZo;	;I"	voff;T;I"	voff;T;@;	iZo;	;I"	blur;T;I"	blur;T;@;	iZo;	;I"spread;T;I"spread;T;@;	iZo;	;I"
color;T;I"
color;T;@;	iZ;@:@splat0;	iZ;
{ ;I"text-shadow;T;0:@children[ [o:Sass::Tree::MixinNode;@:
@args[o;	;I"	none;
T;@;;;iX;iX:@keywords{ ;	I"text-shadow;
T:@splat0:@children[ 